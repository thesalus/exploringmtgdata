$uri = 'https://api.scryfall.com/cards/search?q=is%3Acommander+legal%3Acommander'
$data = @()
$hasMore = $true

do {
  $Response = Invoke-WebRequest -Uri $Uri
  $json = ($Response.Content | ConvertFrom-Json)
  $hasMore = $json.has_more
  $uri = $json.next_page
  $data += $json.data
} while ($hasMore)

$data | ConvertTo-Json -depth 100 | Out-File .\commanders.json
$data | select name, @{name="color_identity"; expression={$_.color_identity -join ', '} }, cmc, @{name="prices_usd"; expression={ $_.prices.usd } } | Export-Csv -Path .\commanders.csv