import java.io.File

// (Get-Content ./AllCards.json | ConvertFrom-Json).psobject.properties.name | Out-File -Encoding utf8 ./AllCardNames

/**
 * Predicate to decide what characters we want to use in our comparisons.
 */
fun isIndexable(char: Char): Boolean {
    return (char in 'a'..'z' || char in 'A'..'Z' || char == 'é')
}

/**
 * Rolling my own Trie for fun, practice and because I want to store the words that terminate at a given node.
 */
data class TrieNode(val char: Char?) {
    private val children: MutableMap<Char, TrieNode> = mutableMapOf()
    private val wordsTerminatingHere: MutableSet<String> = mutableSetOf()

    /**
     * Insert this word into the tree using the suffix starting at the given index.
     */
    fun insert(word: String, startingIndex: Int) {
        val wordTerminatesHere = startingIndex >= word.length
        if (wordTerminatesHere) {
            wordsTerminatingHere.add(word)
        } else {
            val char = word[startingIndex].toLowerCase()
            if (isIndexable(char)) {
                children.getOrPut(char) { TrieNode(char) }
                    .insert(word, startingIndex + 1)
            } else {
                insert(word, startingIndex + 1)
            }
        }
    }

    /**
     * Search for the node corresponding to the given string (if any)
     */
    fun search(word: String, curIndex: Int = 0): TrieNode? {
        if (curIndex >= word.length) {
            return this
        }

        val char = word[curIndex].toLowerCase()
        if (isIndexable(char)) {
            return children[char]?.search(word, curIndex + 1)
        }

        return search(word, curIndex + 1)
    }

    /**
     * This is a poorly formed function but... it gets all the words that terminate at the given
     * node or in any children nodes.
     */
    fun getWordsInTrie(accumulator: MutableSet<String> = mutableSetOf()): MutableSet<String> {
        accumulator.addAll(wordsTerminatingHere)
        children.forEach {
            it.value.getWordsInTrie(accumulator)
        }
        return accumulator
    }
}

fun main(args: Array<String>) {
    val minimumWordSize = 6
    val inputFileName = "/path/to/AllCardNames"
    val outputFileName = "/path/to/FunContainedCards"
    searchForNestedCardNames(minimumWordSize, inputFileName, outputFileName, ::getFun)
}

fun searchForNestedCardNames(minimumWordSize: Int,
                             inputFileName: String,
                             outputFileName: String,
                             cardFilter: (String, String) -> Boolean) {
    val trie = TrieNode(null)
    File(inputFileName).forEachLine(Charsets.US_ASCII) { cardName ->
        if (cardName.length >= minimumWordSize) {
            // Load all suffixes into the trie
            for (i in 0..cardName.length) {
                trie.insert(cardName, i)
            }
        }
    }

    val outputFile = File(outputFileName)
    outputFile.createNewFile()
    outputFile.printWriter().use { outputWriter ->
        File(inputFileName).forEachLine(Charsets.US_ASCII) { cardName ->
            if (cardName.length >= minimumWordSize) {
                val containingCards: Set<String> = trie.search(cardName)?.getWordsInTrie() ?: mutableSetOf()
                val filteredCards = containingCards.filter{x -> cardFilter.invoke(cardName, x)}
                if (filteredCards.isNotEmpty()) {
                    outputWriter.println("$cardName - $filteredCards")
                }
            }
        }
    }
}

private fun getAll(cardName: String, containingCard: String): Boolean {
    return containingCard != cardName
}

private fun getFun(cardName: String, containingCard: String): Boolean {
    return containingCard != cardName && isToken(cardName, containingCard)
}

/**
 * Very rough heuristic to determine if it's a token in another word.
 */
fun isToken(word: String, otherWord: String): Boolean {
    // If this word exists as a case-sensitive match (including non-indexable letters) then ignore
    val matchIndex = otherWord.indexOf(word)
    if (matchIndex != -1) {
        return false
    }

    // If this word exists as a case-insensitive match (only indexable letters) then ignore if it's an affix
    val lowerWord = word.filter(::isIndexable).toLowerCase()
    val lowerOtherWord = otherWord.filter(::isIndexable).toLowerCase()
    val lowerMatchIndex = lowerOtherWord.indexOf(lowerWord)
    val isPrefix = lowerMatchIndex == 0
    val isSuffix = lowerMatchIndex + lowerWord.length == lowerOtherWord.length
    if (isPrefix || isSuffix) {
        return false
    }
    return true
}
