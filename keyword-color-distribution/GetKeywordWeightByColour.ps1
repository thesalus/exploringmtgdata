# This starts with the "Oracle Cards" bulk data file from Scryfall: https://scryfall.com/docs/api/bulk-data
$json = (Get-Content .\oracle-cards.json | ConvertFrom-Json)
$global:COLOUR_BASIS = 'colors' # Use this for card colour
#$global:COLOUR_BASIS = 'color_identity' # Use this for Commander colour identity

$cards_with_keywords = $json | Where {@($_.keywords).count -gt 0} | Select -Property keywords, name, $COLOUR_BASIS
# Explode the cards into each of its keyword & colour combination then assign a weight based on the colours
# e.g., the flying keyword from Judge's Familiar contributes 0.5 to blue and 0.5 to white.
$keyword_colour_combinations = $cards_with_keywords | foreach { $row = $_;
  # If the colour set is empty, use 'C' instead.
  $colors = $row | Select -ExpandProperty $COLOUR_BASIS
  if ($colors.count -eq 0) {
    $colors = @('C')
  }
  $weight = 1.0 / ([math]::Max(1, $colors.count))
  foreach ($keyword in $row.keywords) {
    foreach ($color in $colors) {
      $props = @{keyword=$keyword; color=$color; weight=$weight}
      New-Object -Type PSObject -Prop $props
    }
  }
}
# Sum the weights aggregated by keyword & color
$keyword_weight_by_colour = $keyword_colour_combinations | Group -Property keyword, color | foreach {
  $keyword = ''
  $color = ''
  $totalWeight = 0.0
  foreach ($row in $_.Group) {
    $keyword = $row.keyword
    $color = $row.color
    $totalWeight += $row.weight
  }
  $props = @{keyword=$keyword; color = $color; weight=$totalWeight}
  New-Object -Type PSObject -Prop $props
}
$keyword_weight_by_colour | Export-Csv -Path .\keyword-distribution-by-weight-and-$COLOUR_BASIS.csv -NoTypeInformation

#----

# Now let's calculate which keywords are most frequently-used by colour
$RESULTS_PER_COLOUR = 5

# Calculate frequency-per-colour for each keyword
$frequency = $keyword_weight_by_colour | Group -Property keyword | foreach {
  $counts = @{keyword=$_.Name; W=0; U=0; B=0; R=0; G=0; C=0}
  $highest_weight = -1 # we'll end up taking the first colour we see in the event of any ties (which might be arbitrary)
  foreach ($row in $_.Group) {
    $counts[$row.color] = [math]::Round($row.weight, 2)
    if ($row.weight -gt $highest_weight) {
      $counts['most_frequent_color'] = $row.color
      $highest_weight = $row.weight
    }
  }
  New-Object -Type PSObject -Prop $counts
}

# For each colour find the top 5 keywords that appear the most frequently in that colour (by absolute weight)
'W', 'U', 'B', 'R', 'G', 'C' `
 | ForEach { $color = $_; $frequency | Where {$_.most_frequent_color -eq $color} | Sort -Property $color -Descending | Select -First $RESULTS_PER_COLOUR } `
 | Select 'most_frequent_color', 'keyword', 'W', 'U', 'B', 'R', 'G', 'C' `
 | Export-Csv -Path .\top-5-by-$COLOUR_BASIS.csv -NoTypeInformation