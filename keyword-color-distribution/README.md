### Data Files

#### Keyword Distribution by Colour

```
keyword-distribution-by-weight-and-(color|id)-{DATE}.csv
```

* `keyword`: the M:tG keyword
* `color`: the colour (there's a separate file for Commander colour identity)
  * There are 6 values: one of WUBRG and C for colourless spells
* `weight`: the frequency at which the keyword is seen in a given colour
  * monocolour (and colourless) cards contribute 1 point
  * multicolour cards contribute 1 point split proportionally across teh component colours
  * e.g., the flying keyword from Judge's Familiar contributes 0.5 to blue and 0.5 to white.

#### Top 5 Most Frequently-Used Keyword by Colour

```
top-5-by-(color|id)-{DATE}.csv
```

* `most_frequent_color`: the colour for which the keyword is the most prevalent
* `keyword`: the M:tG keyword
* `W`, `U`, `B`, `R`, `G`, `C`: the weight for each colour (and colourless)

### Validation

I only did a few manual checks:

* Colourless Infect should be 8: https://scryfall.com/search?q=keyword%3Ainfect+c%3Dc
* Red Mill should be 6.3 (repeating): https://scryfall.com/search?q=keyword%3Amill+c%3Ar

### Miscellaneous

* The script was written using PowerShell because I hate myself.
